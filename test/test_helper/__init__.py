import os
import sys
import inspect
from typing import List, Dict, Union
from collections import namedtuple
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.executor.playbook_executor import PlaybookExecutor
from ansible.playbook.play import Play
from ansible.plugins.callback import CallbackBase, stderr
import json
from pprint import pprint

PlayResult = int
PlaybookResult = int

ANSIBLE_DL = None
def get_ansible_dataloader() -> DataLoader:

    global ANSIBLE_DL
    if ANSIBLE_DL is None:
        ANSIBLE_DL = DataLoader()
    return ANSIBLE_DL

Options = namedtuple('Options', [
    'connection', 'module_path', 'forks', 'become', 'become_method', 'become_user', 'extra_vars',
    'check', 'diff', 'remote_user', 'private_key_file', 'listhosts', 'listtasks', 'listtags', 'syntax',
    'ssh_common_args'
])


def run_playbook(playbook_path: str,
                 inventory_manager: InventoryManager = None, inventory_sources: List[str] = None,
                 username: str = None, password: str = None, private_key_file: str = None,
                 become: bool = False, become_user: str = 'root', become_pass: str = None, become_method: str = 'sudo') -> PlaybookResult:

    loader = get_ansible_dataloader()

    inventory = None
    if inventory_manager is not None:
        inventory = inventory_manager
    else:
        if inventory_sources is not None:
            inventory = InventoryManager(loader=loader,  sources=inventory_sources)
        else:
            raise Exception('No inventory data supplied!')
    
    variable_manager = VariableManager(loader=loader, inventory=inventory)

    if not os.path.exists(playbook_path):
        raise Exception("the playbook: %s could not be found" % playbook_path)

    options = Options(connection='ssh', module_path='/path/to/mymodules', forks=100, become=become, become_method=become_method, become_user=become_user, check=False, remote_user=username, private_key_file=private_key_file,
                      listhosts=None, listtasks=None, listtags=None, syntax=None, diff=False, extra_vars=['host_key_checking=False'], ssh_common_args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null')
    passwords = {'conn_pass': password, 'become_pass': become_pass}

    pbex = PlaybookExecutor(playbooks=[playbook_path], inventory=inventory, variable_manager=variable_manager, loader=loader, options=options, passwords=passwords)
    results = pbex.run()

    return results

def run_play(play_name: str, play_tasks: List[Dict] = [], play_hosts: str = 'all', 
             inventory_manager: InventoryManager = None, inventory_sources: List[str] = None,
             username: str = None, password: str = None, private_key_file: str = None,
             become: bool = False, become_user: str = 'root', become_pass: str = None, become_method: str = 'sudo') -> PlayResult:

    loader = get_ansible_dataloader()

    inventory = None
    if inventory_manager is not None:
        inventory = inventory_manager
    else:
        if inventory_sources is not None:
            inventory = InventoryManager(loader=loader,  sources=inventory_sources)
        else:
            raise Exception('No inventory data supplied!')
    
    variable_manager = VariableManager(loader=loader, inventory=inventory)

    options = Options(connection='ssh', module_path='/path/to/mymodules', forks=100, become=become, become_method=become_method, become_user=become_user, check=False, remote_user=username, private_key_file=private_key_file,
                      listhosts=None, listtasks=None, listtags=None, syntax=None, diff=False, extra_vars=['host_key_checking=False'], ssh_common_args='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null')
    passwords = {'conn_pass': password, 'become_pass': become_pass}

    play_source =  dict(
        name = play_name if play_name is not None else inspect.stack()[1][3],
        hosts = play_hosts,
        gather_facts = 'yes',
        tasks = play_tasks,
    )
    play = Play().load(play_source, variable_manager=variable_manager, loader=loader)

    # actually run it
    tqm = None
    try:
        tqm = TaskQueueManager(
                inventory=inventory,
                variable_manager=variable_manager,
                loader=loader,
                options=options,
                passwords=passwords,
                stdout_callback=stderr.CallbackModule(),
            )
        result = tqm.run(play)
    finally:
        if tqm is not None:
            tqm.cleanup()

    return result

def assert_play_result(play_name, *args, **kwargs):

    result = run_play(play_name, *args, **kwargs)
    assert result == 0, "Play: {0} has failed".format(play_name)

def assert_playbook_result(playbook_path, *args, **kwargs):

    result = run_playbook(playbook_path, *args, **kwargs)
    assert result == 0, "Playbook: {0} has failed".format(playbook_path)