import pytest
import vagrant
import os
from pprint import pprint
from . import test_helper
from pathlib import Path
from ansible.inventory.manager import InventoryManager
from urllib.request import urlopen


class TestPlaybookHttpd(object):
    
    @pytest.mark.parametrize('vagrantfile_name', ['web3_ubuntu_Vagrantfile'], scope="function")
    def test_httpd_testpage(self, vagrant_environment: vagrant.Vagrant):

        ansible_basedir = Path(os.path.abspath(__file__)).resolve().parent
        playbook_path = str(ansible_basedir / 'playbooks' / 'httpd.yml')
        vagrant_environment.up(vm_name='web-1')
        ssh_config = vagrant_environment.conf(vm_name='web-1')

        loader = test_helper.get_ansible_dataloader()
        loader.set_basedir(str(ansible_basedir))
        inventory_manager = InventoryManager(loader=loader, sources=None)
        inventory_manager.add_group('all')
        inventory_manager.add_host(ssh_config['HostName'], group='all', port=ssh_config['Port'])

        test_helper.assert_playbook_result(playbook_path, inventory_manager=inventory_manager, become=True, username=ssh_config['User'], password='vagrant')

        # test that Apache is up and serves the default test page
        response = urlopen('http://127.0.0.1:8081/')
        html = response.read().decode('utf-8')
        assert 'It works' in html, 'It doesn\'t work :('
