import pytest
import vagrant
import os
from pprint import pprint
from . import test_helper
from pathlib import Path
from ansible.inventory.manager import InventoryManager


class TestAnsibleSetup(object):
    
    @pytest.mark.parametrize('vagrantfile_name', ['nodes3_ubuntu_Vagrantfile'], scope="function")
    def test_gather_facts(self, vagrant_environment: vagrant.Vagrant):

        ansible_basedir = Path(os.path.abspath(__file__)).resolve().parent
        vagrant_environment.up(vm_name='node-1')
        ssh_config = vagrant_environment.conf(vm_name='node-1')

        tasks = [
            dict(action=dict(module='setup')),
        ]

        loader = test_helper.get_ansible_dataloader()
        loader.set_basedir(str(ansible_basedir))
        inventory_manager = InventoryManager(loader=loader, sources=None)
        inventory_manager.add_group('all')
        inventory_manager.add_host(ssh_config['HostName'], group='all', port=ssh_config['Port'])

        test_helper.assert_play_result('test_gather_facts', play_tasks=tasks, inventory_manager=inventory_manager, username=ssh_config['User'], password='vagrant')
        test_helper.assert_play_result('test_gather_facts_with_become', play_tasks=tasks, inventory_manager=inventory_manager, username=ssh_config['User'], password='vagrant', become=True)
