import pytest
import vagrant
import tempfile
import subprocess
import shutil
import os
import types
from vagrant import compat

TEST_BASE_BOX = "ubuntu/xenial64"
VAGRANT_TD = None


def list_box_names():
    '''
    vagrant.Vagrant
    '''
    listing = compat.decode(subprocess.check_output('vagrant box list --machine-readable', shell=True))
    box_names = []
    for line in listing.splitlines():
        # Vagrant 1.8 added additional fields to the --machine-readable output,
        # so unpack the fields according to the number of separators found.
        if line.count(',') == 3:
            timestamp, _, kind, data = line.split(',')
        else:
            timestamp, _, kind, data, extra_data = line.split(',')
        if kind == 'box-name':
            box_names.append(data.strip())
    return box_names


# SETUP AND TEARDOWN

#region Monkey-patch
def my_ssh_config(self, vm_name=None):
    '''
    Return the output of 'vagrant ssh-config' which appears to be a valid
    Host section suitable for use in an ssh config file.
    Raises an Exception if the Vagrant box has not yet been created or
    has been destroyed.
    vm_name: required in a multi-VM environment.
    Example output:
        Host default
            HostName 127.0.0.1
            User vagrant
            Port 2222
            UserKnownHostsFile /dev/null
            StrictHostKeyChecking no
            PasswordAuthentication no
            IdentityFile /Users/todd/.vagrant.d/insecure_private_key
            IdentitiesOnly yes
    '''
    # capture ssh configuration from vagrant
    command = self._make_vagrant_command(['ssh-config', vm_name])
    output = None
    with self.err_cm() as err_fh:
        try:
            output = subprocess.check_output(command, cwd=self.root, env=self.env, stderr=err_fh)
        except subprocess.CalledProcessError as cpe:
            output = cpe.output
    return compat.decode(output)
#endregion


@pytest.fixture()
def vagrant_environment(vagrantfile_name, vagrantfile_folder=os.path.dirname(os.path.abspath(__file__)) + '/vagrantfiles', vagrant_tmpdir=tempfile.mkdtemp()):
    shutil.copy(os.path.join(vagrantfile_folder,vagrantfile_name), os.path.join(vagrant_tmpdir, 'Vagrantfile'))
    v = vagrant.Vagrant(vagrant_tmpdir)
    # Monkey-patch Vagrant object to quiesce non-0 rc exceptions 
    v.ssh_config = types.MethodType(my_ssh_config, v)
    yield v
    try:
        v.destroy()
    except subprocess.CalledProcessError:
        pass
    finally:
        os.unlink(os.path.join(vagrant_tmpdir, "Vagrantfile"))


@pytest.fixture(scope="class")
def vagrant_tmpdir():
    global VAGRANT_TD
    if VAGRANT_TD is None:
        VAGRANT_TD = tempfile.mkdtemp()
    yield VAGRANT_TD
    shutil.rmtree(VAGRANT_TD)

@pytest.fixture(scope="module")
def vagrant_basebox():
    boxes = list_box_names()
    if TEST_BASE_BOX not in boxes:
        cmd = 'vagrant box add {}'.format(TEST_BASE_BOX)
        subprocess.check_call(cmd, shell=True)
