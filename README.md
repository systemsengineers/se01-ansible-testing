se01-ansible
============


Discover the tests
------------------

```
pytest --collect-only
```

Output:
```
collected 3 items
<Module 'test/test_gather_facts.py'>
  <Class 'TestAnsibleSetup'>
    <Instance '()'>
      <Function 'test_gather_facts[nodes3_ubuntu_Vagrantfile]'>
<Module 'test/test_playbook_httpd.py'>
  <Class 'TestPlaybookHttpd'>
    <Instance '()'>
      <Function 'test_httpd_testpage[web3_ubuntu_Vagrantfile]'>
<Module 'test/test_role_website.py'>
  <Class 'TestRoleWebsite'>
    <Instance '()'>
      <Function 'test_website_txt[web3_ubuntu_Vagrantfile]'>
```

Running all tests
-----------------

```
pytest
```

... printing stdout
```
pytest -s
```

Running one test
----------------

1. Running all tests from a test class
```
pytest -s -k TestPlaybookHttpd
```

2. Selecting tests to run
```
pytest -s -k test_website_txt
```